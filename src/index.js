import express from 'express'
import dbConfig from './config/db'
import middlewaresConfig from './config/middlewares'
import { ToolsRoutes } from './modules'

const app = express()

// dbConfig()

middlewaresConfig(app)

app.use('/api/v1', [ToolsRoutes])

app.get('/', (req, res) => {
  return res.status(200).json({ status: true, message: 'welcome' })
})
const PORT = process.env.PORT || 5005

app.listen(PORT, err => {
  if (err) {
    console.error(err)
  } else {
    console.log(`App listen to port: ${PORT}`)
  }
})
