import bodyParser from 'body-parser'
import morgan from 'morgan'
import passport from 'passport'
import multer from 'multer'

export default app => {
  app.use(bodyParser.json({limit: '50mb', type: 'application/json'}))
  app.use(bodyParser.urlencoded({extended: false}))
  app.use(morgan('dev'))
  app.use(passport.initialize())
  app.use(multer({ dest: 'uploads/' }).single('image'))
}
