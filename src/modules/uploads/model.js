import mongoose, { Schema } from 'mongoose'

const UploadSchema = new Schema({
  publicId: {
    type: String,
    required: true
  },
  signature: {
    type: String
  },
  width: {
    type: Number
  },
  height: {
    type: Number
  },
  format: {
    type: String
  },
  resourceType: {
    type: String
  },
  uploadedAt: {
    type: Date
  },
  bytes: {
    type: Number
  },
  url: {
    type: String
  },
  secureUrl: {
    type: String
  }
}, { timestamps: true })

export default mongoose.model('Upload', UploadSchema)
