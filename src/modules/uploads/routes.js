import { Router } from 'express'
import * as UploadController from './controller'
import { requireJwtAuth } from '../../utils/requireJwtAuth'

const routes = new Router()

routes.post('/uploads', requireJwtAuth, UploadController.uploadImage)

export default routes
