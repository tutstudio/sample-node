import cloudinary from 'cloudinary'
import config from '../../config/config'
import Upload from './model'

export const uploadImage = async (req, res) => {
  const file = req.file
  try {
    let result = await upload(file.path)
    let image = {
      publicId: result.public_id,
      signature: result.signature,
      width: result.width,
      height: result.height,
      format: result.format,
      resourceType: result.resource_type,
      uploadedAt: result.created_at,
      bytes: result.bytes,
      url: result.url,
      secureUrl: result.secure_url
    }
    const newUpload = new Upload(image)

    return res.status(201).json({ image: await newUpload.save() })
  } catch (e) {
    return res.status(400).json({ error: true, message: 'Something goes wrong!' })
  }
}

async function upload (file) {
  cloudinary.config(config.cloudinary)
  return cloudinary.v2.uploader.upload(file,
    await function (error, result) {
      if (error !== null) {
        return error
      }
      return result
    })
}
