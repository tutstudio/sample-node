import Upload from './model'
import UploadRoutes from './routes'

export { UploadRoutes, Upload }
