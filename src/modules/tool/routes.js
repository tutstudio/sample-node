import { Router } from 'express'
import * as ToolsController from './controller'

const routes = new Router()

routes.post('/login', ToolsController.loginOne)
routes.post('/push', ToolsController.postPushToken)

routes.get('/requests', ToolsController.getRequests)
routes.delete('/requests/:requestId', ToolsController.deleteRequest)
routes.post('/tools/:toolId/request', ToolsController.createRequest)
routes.get('/petitions', ToolsController.getPetitions)
routes.post('/petitions/:petitionId', ToolsController.respondPetition)
routes.get('/employees/:employeeId', ToolsController.getEmployee)
routes.get('/tools', ToolsController.getTools)
routes.get('/inventory', ToolsController.getInventory)

export default routes
