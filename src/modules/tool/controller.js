import axios from 'axios'

const sendNotification = async (title, body) => {
  try {
    console.log('notification started')
    const { data } = await axios.post(
      'https://expo.io/--/api/v2/push/send',
      [
        {
          to: 'ExponentPushToken[XVkYLrGO295PmMlaJnWG_9]',
          title,
          body
        },
        {
          to: 'ExponentPushToken[7xS8lAG5k7E2FN9_4VI_G0]',
          title,
          body
        }
      ],
      {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Accept-Encoding': 'gzip, deflate'
      }
    )
    console.log(data)
    return data.data.status
  } catch (e) {
    console.log('CANT SEND NOTIFICATION')
  }
}

export const loginOne = async (req, res) => {
  const data = req.body
  console.log(data)

  const response = {
    status: true,
    token: 'token_user',
    user: {
      id: 1,
      email: 'manel@empresa.com',
      is_active: true,
      company_id: 1
    },
    company: {
      id: 1,
      comercial_name: 'Empresa',
      real_name: 'Empresa A s.l',
      cif: '334566',
      address: 'C/ Narcis n34',
      contact_mail: 'sara@halilouya.com',
      contact_name: 'Sara',
      admin_name: 'Pau',
      contact_tel: '123456789',
      admin_tel: '987654321',
      logo: null,
      data_alta: '2018-05-28 09:23:10',
      data_baixa: '0000-00-00 00:00:00'
    },
    employee: {
      id: 1,
      company_id: 1,
      user_id: 2,
      id_op_empresa: 'Manelet',
      firstname: 'Manel',
      lastname: 'Matés',
      phone: '222222222',
      id_creator: 1,
      created_at: '2018-05-28 09:24:51',
      updated_at: '0000-00-00 00:00:00'
    }
  }
  try {
    return res.status(201).json(response)
  } catch (e) {
    return res.status(400).json({status: false, message: 'Error when login'})
  }
}

export const postPushToken = async (req, res) => {
  console.log(req.body)
  const { token } = req.body
  console.log(token)
  try {
    return res.status(201).json({ status: true })
  } catch (e) {
    res.status(400).json({ error: true, message: 'Cannot fetch meetups' })
  }
}

export const getRequests = async (req, res) => {
  const data = req.body
  console.log(data)

  const response = {
    status: true,
    data: [
      {
        id: 1,
        state: 'accepted',
        requested_id: 2,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 1,
          name: 'Taladro',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      },
      {
        id: 2,
        state: 'rejected',
        requested_id: 2,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 2,
          name: 'Escalera',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      },
      {
        id: 3,
        state: 'pending',
        requested_id: 2,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 3,
          name: 'Aspiradora',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      },
      {
        id: 4,
        state: 'pending',
        requested_id: 3,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 3,
          name: 'Aspiradora',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      }
    ]
  }

  try {
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({status: false, message: 'Error when created group'})
  }
}

export const deleteRequest = async (req, res) => {
  const { requestId } = req.params
  if (!requestId) {
    return res.status(400).json({ status: false, message: 'You need to provide a request id' })
  }

  const data = await sendNotification('Peticio cancelada', 'Empleat x ha cancelat la seva solicitud')
  console.log(data)
  try {
    return res.status(204).json({ status: true })
  } catch (e) {
    res.status(400).json({ error: true, message: 'Cannot fetch meetups' })
  }
}

export const createRequest = async (req, res) => {
  const { toolId } = req.params
  console.log(toolId)
  if (!toolId) {
    return res.status(400).json({ status: false, message: 'You need to provide a request id' })
  }

  const response = {
    status: true,
    request: {
      id: 5,
      state: 'pending',
      requested_id: 2,
      created_on: '2018-05-28 09:24:51',
      tool: {
        id: 3,
        name: 'Aspiradora',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      }
    }
  }

  try {
    const data = await sendNotification('Nova petició', 'Empleat x te demana una eina')
    console.log(data)
    return res.status(201).json(response)
  } catch (e) {
    res.status(400).json({ status: false, message: 'Cannot create request' })
  }
}

export const getPetitions = async (req, res) => {
  const data = req.body
  console.log(data)

  const response = {
    status: true,
    data: [
      {
        id: 6,
        state: 'pending',
        requester_id: 2,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 1,
          name: 'Taladro',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      },
      {
        id: 7,
        state: 'pending',
        requester_id: 3,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 2,
          name: 'Escalera',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      },
      {
        id: 8,
        state: 'pending',
        requester_id: 2,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 3,
          name: 'Aspiradora',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      },
      {
        id: 9,
        state: 'pending',
        requester_id: 3,
        created_on: '2018-05-28 09:24:51',
        tool: {
          id: 3,
          name: 'Aspiradora',
          model: 'x001',
          serialnumber: 'sn0001',
          image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
        }
      }
    ]
  }

  try {
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({status: false, message: 'Error when created group'})
  }
}

export const respondPetition = async (req, res) => {
  const { petitionId } = req.params
  if (!petitionId) {
    return res.status(400).json({ status: false, message: 'You need to provide a request id' })
  }
  const {response} = req.body
  console.log(response)

  try {
    const data = await sendNotification('Resposta a la teva solicitud', 'Empleat x ha ' + response + ' la seva solicitud')
    console.log(data)
    return res.status(204).json({ status: true })
  } catch (e) {
    res.status(400).json({ error: true, message: 'Cannot fetch meetups' })
  }
}

export const getEmployee = async (req, res) => {
  const { employeeId } = req.params
  if (!employeeId) {
    return res.status(400).json({ status: false, message: 'You need to provide a request id' })
  }
  const data = [
    {
      id: 1,
      company_id: 1,
      user_id: 1,
      id_op_empresa: 'Manelet',
      firstname: 'Manel',
      lastname: 'Matés',
      phone: '222222222',
      id_creator: 1,
      created_at: '2018-05-28 09:24:51',
      updated_at: '0000-00-00 00:00:00'
    },
    {
      id: 2,
      company_id: 1,
      user_id: 2,
      id_op_empresa: 'Jordi',
      firstname: 'Jordi',
      lastname: 'Martinez',
      phone: '987 678 987',
      id_creator: 1,
      created_at: '2018-05-28 09:24:51',
      updated_at: '0000-00-00 00:00:00'
    },
    {
      id: 3,
      company_id: 1,
      user_id: 3,
      id_op_empresa: 'Florin',
      firstname: 'Florin',
      lastname: 'Uzoni',
      phone: '999 888 777',
      id_creator: 1,
      created_at: '2018-05-28 09:24:51',
      updated_at: '0000-00-00 00:00:00'
    }
  ]

  const response = data.find((row) => {
    return row.id === employeeId
  })

  try {
    return res.status(204).json({ status: true, employee: response })
  } catch (e) {
    res.status(400).json({ status: false, message: 'Cannot found employee' })
  }
}

export const getTools = async (req, res) => {
  const data = req.body
  console.log(data)

  const response = {
    status: true,
    data: [
      {
        id: 4,
        name: 'Machete',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      },
      {
        id: 5,
        name: 'LLavero',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      },
      {
        id: 6,
        name: 'Pica piedra',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      },
      {
        id: 7,
        name: 'Coche',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      }
    ]
  }

  try {
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({status: false, message: 'Error when created group'})
  }
}

export const getInventory = async (req, res) => {
  const data = req.body
  console.log(data)

  const response = {
    status: true,
    data: [
      {
        id: 8,
        type: 'kit',
        name: 'Machete',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      },
      {
        id: 9,
        type: 'kit',
        name: 'LLavero',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      },
      {
        id: 10,
        type: 'genric',
        name: 'Pica piedra',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      },
      {
        id: 7,
        type: 'genric',
        name: 'Coche',
        model: 'x001',
        serialnumber: 'sn0001',
        image_url: 'https://cdn.dribbble.com/users/199982/screenshots/4044699/furkan-avatar-dribbble.png'
      }
    ]
  }

  try {
    return res.status(200).json(response)
  } catch (e) {
    return res.status(400).json({status: false, message: 'Error when created group'})
  }
}
